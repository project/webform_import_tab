<?php

namespace Drupal\webform_import_tab\Form;

use Drupal\Component\Serialization\Yaml;
use Drupal\config\Form\ConfigSingleImportForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form for importing a single webforms.
 *
 * @internal
 */
class WebformImport extends ConfigSingleImportForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_import_tab_import';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('webform_import_tab.import');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form['config_type'] = [
      '#type' => 'hidden',
      '#default_value' => 'webform',
    ];

    // When this is the confirmation step fall through to the confirmation form.
    if ($this->data) {
      return parent::buildForm($form, $form_state);
    }

    $form['config_type'] = [
      '#type' => 'hidden',
      '#default_value' => 'webform',
      '#required' => TRUE,
    ];
    $form['author'] = [
      '#title' => $this->t('Who should be attributed as the author of this webform?'),
      '#description' => $this->t('Webform imports contain a User ID to identify the author, which may be different or not exist on this site.'),
      '#type' => 'radios',
      '#default_value' => 1,
      '#options' => [
        0 => $this->t('Use the user ID as listed in my import file (recommended if importing a webform to the same site it was exported from)'),
        1 => $this->t('Change the author user ID to make me the author (recommended if importing a webform from another site)'),
      ],
    ];
    $form['import'] = [
      '#title' => $this->t('Paste your configuration here'),
      '#type' => 'textarea',
      '#rows' => 24,
      '#required' => TRUE,
    ];
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if ($form_state->getValue('config_type') != 'webform') {
      $form_state->setErrorByName('import', $this->t('Only webforms can be imported here.'));
    }

    // $form['author'] = 'Change to my uid'
    if ($form['author']['#value'] === '1' && $form_state->getValue(['import'])) {
      $config = $form_state->getValue(['import']);
      $yaml_decoded = Yaml::decode($config);

      $uid = \Drupal::currentUser()->id();

      $yaml_decoded['uid'] = $uid;
      $yaml = Yaml::encode($yaml_decoded);
      $form_state->setValue(['import'], $yaml);
    }

    parent::validateForm($form, $form_state);
    if ($this->configExists && !$this->configExists->access('update')) {
      $form_state->setErrorByName('import', $this->t('Access denied to the form being updated.'));
    }
  }

}
