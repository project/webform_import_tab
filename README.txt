# Webform Import Tab

This module adds a tab under structure/webforms that user who can create webforms may access.

From there, they can import webforms only.
